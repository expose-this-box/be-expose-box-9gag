var express = require('express');
const { MongoClient } = require('mongodb');
const crypto = require('crypto');

var router = express.Router();


router.post('/login', async (req, res, next) => {
	const { name, password } = req.body;
	const url = 'mongodb://exposeboxuser:1q2w3e@ds241968.mlab.com:41968/exposebox';
	try {
		const client = await new MongoClient.connect(url, { 'useUnifiedTopology': true });
		const db = client.db();
		const SECRET = 'demo';
		const hash = crypto.createHash('sha256').update(password + SECRET).digest('hex');

		const doc = await db.collection('users').findOne({ 'username': name, 'password': hash });

		if (!doc) {
			return res.status(401).send({ 'message': 'Wrong user+pasword combination' });
		}

		res.status(200).send({ name, 'id': doc._id });
	} catch (error) {
		console.error('error is: ', error);
	}
});


module.exports = router;
