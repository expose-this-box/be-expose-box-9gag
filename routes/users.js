var express = require('express');
const { MongoClient, ObjectID } = require('mongodb');

var router = express.Router();

/* UPDATE users top queries. */
router.post('/update-top-queries', async (req, res, next) => {
	const {
		id, queryKey,
	} = req.body;
	const url = 'mongodb://exposeboxuser:1q2w3e@ds241968.mlab.com:41968/exposebox';

	try {
		const client = await new MongoClient.connect(url, { 'useUnifiedTopology': true });
		const db = client.db();

		await db.collection('users')
			.updateOne(
				{ '_id': ObjectID(id) },
				{
					'$inc': { [`queries.${queryKey}`]: 1 },
				},
			);
	} catch (error) {
		console.error('error is: ', error);
		return res.status(500).send({ 'message': 'Failed to increment' });
	}

	res.status(200).send({});
});

/* GET users top10 queries. */
router.post('/get-top-queries', async (req, res, next) => {
	const {	id } = req.body;
	const url = 'mongodb://exposeboxuser:1q2w3e@ds241968.mlab.com:41968/exposebox';

	try {
		const client = await new MongoClient.connect(url, { 'useUnifiedTopology': true });
		const db = client.db();
		const result = await db.collection('users')
			.aggregate([
				{
					'$match': { '_id': ObjectID(id) },
				},
				{
					'$project': {
						'queries': { '$objectToArray': '$queries' },
					},
				},
				{
					'$unwind': '$queries',
				},
				{ '$sort': { 'queries.v': -1 } },
				{ '$group': { '_id': '$_id', 'queries': { '$push': '$queries' } } },
				{
					'$project': {
						'top10Queries': { '$slice': ['$queries', 10] },
					},
				},
			])
			.toArray();

		if (result[0]) {
			return res.status(200).send({ 'results': result[0] });
		}

		return res.status(500).send({ 'message': 'server error1' });
	} catch (error) {
		console.error('error is: ', error);
		return res.status(500).send({ 'message': 'server error2' });
	}
});

module.exports = router;
